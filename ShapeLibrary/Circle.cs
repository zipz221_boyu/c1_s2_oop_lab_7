﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapeLibrary
{
    public class Circle : Dot {

        private int radius;

        public int Radius {
            get => radius;
            set => radius = value < 1 ? 1 : value;
        }

        public Circle() : base() => Radius = random.Next(1, MAX_XY);
        public Circle(int x, int y, int radius) : base(x, y) => Radius = radius;
        public Circle(int x, int y, int radius, int strokeWidth) : base(x, y, strokeWidth) => Radius = radius;
        public Circle(int x, int y, int radius, int strokeWidth, Color color) : base(x, y, strokeWidth, color) => Radius = radius;
        public Circle(Circle it) : base(it) => Radius = it.radius;

        public override void Draw(Graphics graphics) {
            graphics.DrawEllipse(Pen, X, Y, Radius, Radius);
        }
    }
}
