﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapeLibrary {
    public class Ellipse : Circle {

        private int secondRadius;

        public int SecondRadius {
            get => secondRadius;
            set => secondRadius = value < 1 ? 1 : value;
        }

        public Ellipse() : base() => SecondRadius = random.Next(1, MAX_XY);
        public Ellipse(int x, int y, int radius, int secondRadius)
            : base(x, y, radius) => SecondRadius = secondRadius;
        public Ellipse(int x, int y, int radius, int secondRadius, int strokeWidth)
            : base(x, y, radius, strokeWidth) => SecondRadius = secondRadius;
        public Ellipse(int x, int y, int radius, int secondRadius, int strokeWidth, Color color)
            : base(x, y, radius, strokeWidth, color) => SecondRadius = secondRadius;
        public Ellipse(Ellipse it) : base(it) => SecondRadius = it.SecondRadius;

        public override void Draw(Graphics graphics) {
            graphics.DrawEllipse(Pen, X, Y, Radius, SecondRadius);
        }
    }
}
