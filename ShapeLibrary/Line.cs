﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapeLibrary {
    public class Line : Dot {

        private int secondX = 1;
        private int secondY = 1;

        public virtual int SecondX {
            get => secondX;
            set => secondX = value < 0 ? 0 : value;
        }
        public virtual int SecondY {
            get => secondY;
            set => secondY = value < 0 ? 0 : value;
        }

        public Line() : base() {
            SecondX = random.Next(1, MAX_XY);
            SecondY = random.Next(1, MAX_XY);
        }

        public Line(int x, int y, int secondX, int secondY) : base(x, y) {
            SecondX = secondX;
            SecondY = secondY;
        }

        public Line(int x, int y, int secondX, int secondY, int strokeWidth) : base(x, y, strokeWidth) {
            SecondX = secondX;
            SecondY = secondY;
        }

        public Line(int x, int y, int secondX, int secondY, int strokeWidth, Color color) : base(x, y, strokeWidth, color) {
            SecondX = secondX;
            SecondY = secondY;
        }

        public Line(Line it) : base(it) {
            SecondX = it.SecondX;
            SecondY = it.SecondY;
        }

        public override void Draw(Graphics graphics) {
            graphics.DrawLine(Pen, X, Y, SecondX, SecondY);
        }
    }
}
