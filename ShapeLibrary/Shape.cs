﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapeLibrary {
    public abstract class Shape {

        protected static Random random = new Random();
        protected const int MAX_XY = 430;

        private int x = 1;
        private int y = 1;

        public int X {
            get => x;
            set => x = value < 0 ? 0 : value;
        }
        public int Y {
            get => y;
            set => y = value < 0 ? 0 : value;
        }
        public int StrokeWidth { get; set; } = 1;
        public Color Color { get; set; } = Color.Black;

        protected Pen Pen { get => new Pen(Color, StrokeWidth); }

        public Shape() {
            StrokeWidth = random.Next(1, 10);
            X = random.Next(0, MAX_XY);
            Y = random.Next(0, MAX_XY);
            Color = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
        }

        public Shape(int x, int y) : this() {
            X = x;
            Y = y;
        }

        public Shape(int x, int y, int strokeWidth) : this(x, y) {
            StrokeWidth = strokeWidth;
        }

        public Shape(int x, int y, int strokeWidth, Color color) : this(x, y, strokeWidth) {
            Color = color;
        }

        public Shape(Shape it) : this(it.X, it.Y, it.StrokeWidth, it.Color) { }

        public abstract void Draw(Graphics graphics);
    }
}
