﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapeLibrary
{
    public class Dot : Shape {

        public Dot() : base() { }
        public Dot(int x, int y) : base(x, y) { }
        public Dot(int x, int y, int strokeWidth) : base(x, y, strokeWidth) { }
        public Dot(int x, int y, int strokeWidth, Color color) : base(x, y, strokeWidth, color) { }
        public Dot(Dot it) : base(it) { }

        public override void Draw(Graphics graphics) {
            graphics.DrawEllipse(Pen, X, Y, StrokeWidth, StrokeWidth);
        }
    }
}
