﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapeLibrary {
    public class Rectangle : Line {

        private int width = 1;
        private int height = 1;

        public int Width {
            get => width;
            set {
                width = value;
                int it = X + width;
                if (it != SecondX) base.SecondX = it;
            }
        }
        public int Height {
            get => height;
            set {
                height = value;
                int it = Y + height;
                if (it != SecondY) base.SecondY = it;
            }
        }

        public override int SecondX {
            get => base.SecondX;
            set {
                base.SecondX = value;
                int it = base.SecondX - X;
                if (it != Width) Width = it;
            }
        }
        public override int SecondY {
            get => base.SecondY;
            set {
                base.SecondY = value;
                int it = base.SecondY - Y;
                if (it != Height) Height = it;
            }
        }

        public Rectangle() : base() {
            Width = SecondX - X;
            Height = SecondY - Y;
        }

        public Rectangle(int x, int y, int width, int height)
            : base(x, y, x + width, y + height) {
            Width = width;
            Height = height;
        }

        public Rectangle(int x, int y, int width, int height, int strokeWidth)
            : base(x, y, x + width, y + height, strokeWidth) {
            Width = width;
            Height = height;
        }

        public Rectangle(int x, int y, int width, int height, int strokeWidth, Color color)
            : base(x, y, x + width, y + height, strokeWidth, color) {
            Width = width;
            Height = height;
        }

        public Rectangle(Rectangle it) : base(it) {
            Width = it.Width;
            Height = it.Height;
        }

        public override void Draw(Graphics graphics) {
            graphics.DrawRectangle(Pen, X, Y, Width, Height);
        }
    }
}
