﻿using ShapeLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShapeFormsApp {
    public partial class ShapeForm : Form {
        public ShapeForm() {
            InitializeComponent();
        }

        private void clearButtonClick(object sender, EventArgs e) {
            shapePictureBox.CreateGraphics().Clear(System.Drawing.SystemColors.Control);
        }

        private void drawButtonClick(object sender, EventArgs e) {
            Graphics graphics = shapePictureBox.CreateGraphics();
            Shape[] shapes = new Shape[20];
            Random rand = new Random();

            for (int i = 0; i < shapes.Length; i++) {
                switch (rand.Next() % 5) {
                    case 0:
                        shapes[i] = new Line();
                        break;
                    case 1:
                        shapes[i] = new ShapeLibrary.Rectangle();
                        break;
                    case 2:
                        shapes[i] = new Circle();
                        break;
                    case 3:
                        shapes[i] = new Ellipse();
                        break;
                    default:
                        shapes[i] = new Dot();
                        break;
                }

                shapes[i].Draw(graphics);
            }

        }
    }
}
