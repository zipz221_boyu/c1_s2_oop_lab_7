﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonLibrary {

    public class Person {
        
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime DateOfBirth { get; set; }

        protected Person() { }

        public Person(string name, string surname, DateTime dateOfBirth) {
            Name = name;
            Surname = surname;
            DateOfBirth = dateOfBirth;
        }

        public Person(string name, string surname, int yearOfBirth, int monthOfBirth, int dayOfBirth)
            : this(name, surname, new DateTime(yearOfBirth, monthOfBirth, dayOfBirth)) { }

        public Person(Person it) : this(it.Name, it.Surname, it.DateOfBirth) { }

        public override string ToString() {
            string it = $"Ім'я : {Name}\n";
            it += $"Прізвище : {Surname}\n";
            it += $"Дата народження : {DateOfBirth.ToString("dd.MM.yyyy")}";
            return it;
        }
    }
}
