﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PersonLibrary {

    public class Teacher : LibraryUser {

        public string Position { get; set; }
        public string Department { get; set; }
        public string NameInstitution { get; set; }

        private Teacher() : base() { }

        public Teacher(LibraryUser it, string nameInstitution, string position, string department) : base(it) {
            Position = position;
            Department = department;
            NameInstitution = nameInstitution;
        }

        public Teacher(LibraryUser it, string nameInstitution, string department)
            : this(it, nameInstitution, "Викладач", department) { }

        public Teacher(Teacher it) : this(it, it.NameInstitution, it.Position, it.Department) { }


        public override string ToString() {
            string it = base.ToString() + "\n\n";
            it += $"Вищий навчальний заклад : {NameInstitution}\n";
            it += $"Посада : {Position}\n";
            it += $"Кафедра : {Department}";
            return it;
        }

    }
}
