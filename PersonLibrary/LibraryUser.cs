﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace PersonLibrary {

    public class LibraryUser : Person {

        public int ReaderTicketNumber { get; set; }
        public int AmountMonthlyReadingFee { get; set; }
        public DateTime DateOfIssue { get; set; }

        protected LibraryUser() { }

        public LibraryUser(Person it, int readerTicketNumber, int amountMonthlyReadingFee, DateTime dateOfIssue) {
            ReaderTicketNumber = readerTicketNumber;
            AmountMonthlyReadingFee = amountMonthlyReadingFee;
            DateOfIssue = dateOfIssue;
        }

        public LibraryUser(Person it, int readerTicketNumber, int amountMonthlyReadingFee, int yearOfIssue, int monthOfIssue, int dayOfIssue)
            : this(it, readerTicketNumber, amountMonthlyReadingFee, new DateTime(yearOfIssue, monthOfIssue, dayOfIssue)) { }

        public LibraryUser(LibraryUser it) : this(it, it.ReaderTicketNumber, it.AmountMonthlyReadingFee, it.DateOfIssue) { }

        public override string ToString() {
            string it = base.ToString() + "\n\n";
            it += $"Номер читацького квитка : {ReaderTicketNumber}\n";
            it += $"Дата видачі : {DateOfIssue.ToString("dd.MM.yyyy")}\n";
            it += $"Розмірщомісячного читацького внеску : {AmountMonthlyReadingFee} грн";
            return it;
        }
    }
}
