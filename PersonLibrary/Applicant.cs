﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonLibrary {

    public class Applicant : Person {

        private double averageScoreForDocEducation = 0;

        public List<ScoreForZNO> ScoreForZNO { get; } = new List<ScoreForZNO>();
        public string SchoolName { get; set; }
        public double AverageScoreForDocEducation {
            get => averageScoreForDocEducation;
            set => averageScoreForDocEducation = value < 0 ? 0 : value > 100 ? 100 : value;
        }

        private Applicant() : base() { }

        public Applicant(Person it, string schoolName, double averageScoreForDocEducation,IEnumerable<ScoreForZNO> scoreForZNO) : base(it) {
            AverageScoreForDocEducation = averageScoreForDocEducation;
            SchoolName = schoolName;
            ScoreForZNO.AddRange(scoreForZNO);
        }

        public Applicant(Person it, string schoolName, double averageScoreForDocEducation) 
            : this(it, schoolName, averageScoreForDocEducation, new ScoreForZNO[0]) { }

        public Applicant(Applicant it) : this(it, it.SchoolName, it.AverageScoreForDocEducation, it.ScoreForZNO) { }

        public void addScoreForZNO(ScoreForZNO item) => ScoreForZNO.Add(item);

        public override string ToString() {
            string it = base.ToString() + "\n\n";
            it += $"Назва загальноосвітнього навчального закладу : {SchoolName}\n";
            it += $"Кількість балів за документ про освіту : {AverageScoreForDocEducation}\n";
            it += "Сертифікати ЗНО :";

            if (ScoreForZNO == null || !ScoreForZNO.Any()) it += " відсутні!!!";
            else foreach (ScoreForZNO item in ScoreForZNO) it += $"\n\t{item.ToString()}\n";

            return it;
        }
    }
}
