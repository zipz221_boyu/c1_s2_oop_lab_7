﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonLibrary {

    public class ScoreForZNO {

        private int scopre = 0;
        private string subject = "";

        public int Scopre {
            get => scopre;
            set => scopre = value < 0 ? 0 : value > 200 ?  200 : value;
        }
        public string Subject {
            get => subject;
            set => subject = value;
        }

        private ScoreForZNO() { }

        public ScoreForZNO(string subject, int scopre) {
            Scopre = scopre;
            Subject = subject;
        }

        public ScoreForZNO(string subject) : this(subject, 0) { }

        public override string ToString() {
            return $"{Subject} : {Scopre}";
        }
    }
}
