﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonLibrary {

    public class Student : LibraryUser {

        public int course = 1;

        public int Course {
            get => course;
            set => course = value < 1 ? 1 : value > 5 ? 5 : 0;
        }
        public string Group { get; set; }
        public string Faculty { get; set; }
        public string NameInstitution { get; set; }

        private Student() : base() { }

        public Student(LibraryUser it, string nameInstitution, string faculty, string group, int course) : base(it) {
            Course = course;
            Group = group;
            Faculty = faculty;
            NameInstitution = nameInstitution;
        }

        public Student(LibraryUser it, string nameInstitution, string faculty, string group)
            : this(it, nameInstitution, faculty, group, 1) { }

        public Student(Student it) : this(it, it.NameInstitution, it.Faculty, it.Group, it.Course) { }

        public override string ToString() {
            string it = base.ToString() + "\n\n";
            it += $"Вищий навчальний заклад : {NameInstitution}\n";
            it += $"Факультет : {Faculty}\n";
            it += $"Група : {Group}\n";
            it += $"Курс : {Course}";
            return it;
        }
    }
}
