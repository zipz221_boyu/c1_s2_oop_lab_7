﻿using PersonLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonConsoleApp {
    internal class PersonProgram {

        private const ConsoleColor defTextColor = ConsoleColor.White;
        private const ConsoleColor errorColor = ConsoleColor.Red;
        private const ConsoleColor titleColor = ConsoleColor.Green;
        private const ConsoleColor hintColor = ConsoleColor.Yellow;

        static void Main(string[] args) {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Console.Title = "Лабораторна робота No7. Завдання 1";
            Console.ForegroundColor = defTextColor;

            start(new List<Person>());
        }

        private static void start(List<Person> items) {
            printMenuTitle("Меню");
            int select = selectMenuItem(new string[] {
                "Вихід",
                "додати людину",
                "показати список людей",
                "показати людину за номером"
            });

            Console.Clear();
            switch (select) {
                case 0:
                    return;
                case 1:
                    printMenuTitle("додати людину");
                    addPerson(items);
                    Console.Clear();
                    break;
                case 2:
                    printMenuTitle("показати список людей");
                    ShowInfo(items);
                    break;
                case 3:
                    printMenuTitle("показати людину за номером");
                    if (items == null || !items.Any()) writeLine("Список порожній!!!", hintColor);
                    else ShowInfo(items, inputInt($"номер людини від 1 до {items.Count}", 1, items.Count));
                    break;
            }
            start(items);
        }

        private static void addPerson(List<Person> items) {
            printMenuTitle("додати");

            Person person = new Person(
                inpuString("ім'я"),
                inpuString("прізвище"),
                inputInt("рік народження \"yyyy\"", min: 0),
                inputInt("місяць народження \"MM\"", min: 1, max: 12),
                inputInt("день народження \"dd\"", min: 1, max: 31)
            );

            writeLine("\nЗберегти людину як:");
            int select = selectMenuItem(new string[] {
                "Вихід",
                "людина",
                "Абітурієнт",
                "Студент",
                "Викладач"
            });

            Console.Clear();
            switch (select) {
                case 0:
                    return;
                case 1:
                    items.Add(person);
                    break;
                case 2:
                    items.Add(createApplicant(person));
                    break;
                case 3:
                    items.Add(createStudent(person));
                    break;
                case 4:
                    items.Add(createSTeacher(person));
                    break;
                default:
                    addPerson(items);
                    break;
            }
        }


        private static Applicant createApplicant(Person person) {
            printTitle("дані абітурієнта", 40, hintColor);

            return new Applicant(
                person,
                inpuString("назву загальноосвітнього навчального закладу"),
                inputDouble("середній бал з документа про освіту", min: 0, max: 100),
                scoreForZNO(new List<ScoreForZNO>())
            );
        }

        private static List<ScoreForZNO> scoreForZNO(List<ScoreForZNO> items) {
            writeLine("Додати оцінку з ЗНО ?");
            int select = selectMenuItem(new string[] { "ні", "так" });

            if (select == 0) return items;
            else if (select == 1) items.Add(new ScoreForZNO(inpuString("назва предмету"), inputInt("оцінка", min: 0, max: 200)));

            return scoreForZNO(items);
        }

        private static Student createStudent(Person person) {
            printTitle("дані студента", 40, hintColor);

            string nameInstitution = inpuString("назву вищого навчального закладу");
            string faculty = inpuString("назву факультета");
            string group = inpuString("назву групи");
            int course = inputInt("курс", min: 1, max: 5);
            return new Student(createLibraryUser(person), nameInstitution, faculty, group, course);
        }

        private static Teacher createSTeacher(Person person) {
            printTitle("дані викладача", 40, hintColor);

            string nameInstitution = inpuString("назву вищого навчального закладу");
            string position = inpuString("посада");
            string department = inpuString("кафедра");
            return new Teacher(createLibraryUser(person), nameInstitution, position, department);
        }

        private static LibraryUser createLibraryUser(Person person) {
            printTitle("дані користувача бібліотеки", 40, hintColor);
            return new LibraryUser(
                person,
                inputInt("номер читацького квитка", min: 1),
                inputInt("розмір щомісячного читацького внеску в грн", min: 1),
                inputInt("рік видачі \"yyyy\"", min: 0),
                inputInt("місяць видачі \"MM\"", min: 1, max: 12),
                inputInt("день видачі \"dd\"", min: 1, max: 31)
            );
        }

        private static void ShowInfo(List<Person> items) {
            if (items == null || !items.Any()) writeLine("Список порожній!!!", hintColor);
            else for (int i = 0; i < items.Count; i++) ShowInfo(items, i + 1);
        }

        private static void ShowInfo(List<Person> items, int number) {
            if (items == null || !items.Any() || number < 1 || number > items.Count) return;

            Person it = items[number - 1];
            printTitle($"No{number}", 40, hintColor);
            writeLine(it.ToString());
            printTitle("", 40, hintColor);
        }

        private static int inputInt(string name, int min = int.MinValue, int max = int.MaxValue) {
            Console.WriteLine($"Вкажіть {name} та натисніть Enter");

            string value = Console.ReadLine();

            if (string.IsNullOrEmpty(value)) {
                writeLine($"\n{name} не може бути порожнім. Будь ласка, повторіть спробу!!!", errorColor);
                return inputInt(name, min, max);
            }

            try {
                int n = int.Parse(value);

                if (n < min) writeLine($"\nЗначення не може бути меншим за {min}. Будь ласка, повторіть спробу!!!", errorColor);
                else if (n > max) writeLine($"\nЗначення не може бути білшим за {max}. Будь ласка, повторіть спробу!!!", errorColor);
                else return n;

                return inputInt(name, min, max);
            } catch (Exception e) {
                writeLine();
                writeLine(
                    (e is System.FormatException ? "Помилка формату введення занчення" : "Сталася невідома помилка") +
                        ". Будь ласка, повторіть спробу!!!",
                    errorColor
                );
                return inputInt(name, min, max);
            }
        }

        private static double inputDouble(string name, double min = double.MinValue, double max = double.MaxValue) {
            Console.WriteLine($"Вкажіть {name} та натисніть Enter");

            string value = Console.ReadLine();

            if (string.IsNullOrEmpty(value)) {
                writeLine($"\n{name} не може бути порожнім. Будь ласка, повторіть спробу!!!", errorColor);
                return inputDouble(name, min, max);
            }

            try {
                double n = double.Parse(value);

                if (n < min) writeLine($"\nЗначення не може бути меншим за {min}. Будь ласка, повторіть спробу!!!", errorColor);
                else if (n > max) writeLine($"\nЗначення не може бути білшим за {max}. Будь ласка, повторіть спробу!!!", errorColor);
                else return n;

                return inputDouble(name, min, max);
            } catch (Exception e) {
                writeLine();
                writeLine(
                    (e is System.FormatException ? "Помилка формату введення занчення" : "Сталася невідома помилка") +
                        ". Будь ласка, повторіть спробу!!!",
                    errorColor
                );
                return inputDouble(name, min, max);
            }
        }

        private static string inpuString(string name) {
            writeLine($"Вкажіть {name} та натисніть Enter");
            string value = Console.ReadLine();
            if (!string.IsNullOrEmpty(value)) return value;

            writeLine("Значення не може бути порожнім. Будь ласка, повторіть спробу!!!", errorColor);
            return inpuString(name);
        }

        private static int selectMenuItem(string[] items, int def = -1) {
            if (items == null || items.Length == 0) return def;

            writeLine("\nВкажіть номер пункту та натисніть Enter");

            for (int i = 1; i < items.Length; i++) printMenuItem(i, items[i]);
            printMenuItem(0, items[0]);
            try {
                int it = int.Parse(Console.ReadLine());
                if (it >= 0 && it < items.Length) return it;
                throw new System.FormatException();
            } catch (Exception) {
                writeLine("\nЗробіть свій вибір!!!", errorColor);
            }

            return selectMenuItem(items, def);
        }

        private static void printMenuItem(int number, string name) {
            write($"\t{number}", hintColor);
            writeLine($" -> {name}.");
        }

        private static void printMenuTitle(string name) {
            printTitle(name, 120, titleColor);
        }

        private static void printTitle(string name, int spaseLenght, ConsoleColor color = defTextColor) {
            string it = $"----------{name}";
            for (int i = it.Length; i < spaseLenght; i++) it += "-";
            writeLine();
            writeLine(it, color);
            writeLine();
        }

        private static void writeLine(string value = "", ConsoleColor color = defTextColor) {
            write($"{value}\n", color);
        }

        private static void write(string value = "", ConsoleColor color = defTextColor) {
            Console.ForegroundColor = color;
            Console.Write(value);
            Console.ForegroundColor = defTextColor;
        }
    }
}
